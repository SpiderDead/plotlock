package com.spiderdead.plotlock.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class EventsClass implements Listener {
    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Action action = event.getAction();
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        if(action.equals(Action.RIGHT_CLICK_BLOCK) || action.equals(Action.LEFT_CLICK_BLOCK)){
            if(block.getType().equals(Material.CHEST)){
                if(!player.hasPermission("plotlock.override")){
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "You are not allowed to open this chest!");
                }else{
                    player.sendMessage(ChatColor.AQUA + "You clicked on a chest!");
                }
            }
        }
    }
}
