package com.spiderdead.plotlock;

import com.spiderdead.plotlock.events.EventsClass;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class plotlock extends JavaPlugin {

    @Override
    public void onEnable(){
        getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "\n\n\nPlotLock has been enabled!\n\n");
        getServer().getPluginManager().registerEvents(new EventsClass(), this);
    }

    @Override
    public void onDisable(){
        getServer().getConsoleSender().sendMessage(ChatColor.RED + "\n\n\nPlotLock has been disabled..\n\n");
    }
}
